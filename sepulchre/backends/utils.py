"""Backend utility functions."""

import importlib
import os
import sys

from . import ABCBackend


def load_backend(config: dict = None) -> ABCBackend:
    """Load given backend and call it's setup function."""
    if config is None:
        config = {}

    old_path = sys.path

    default_backends = [os.path.dirname(__file__)]

    sys.path.extend(default_backends + config.get("plugin_dirs", []))

    backend_module = importlib.import_module(config.get("name", "local_disk"))

    sys.path = old_path

    return backend_module.setup()
