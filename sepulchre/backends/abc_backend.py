"""Define Abstract base class for Backend plugins to use.

"""

import abc


class ABCBackend(abc.ABC):
    """Defines interface for all backend classes to inherit from.

    BACKEND INTERFACE _snigger_
    ---------------------------

    The following methods must be defined:

        save(tomb: bytes)
        load(tomb: bytes)

    In both cases the name param is the name of the tomb to save and tomb
    is the encrypted contents of the tomb as bytes.
    """

    @abc.abstractmethod
    def save(self, encrypted_data: bytes) -> bool:
        """Abstract method that must save encrypted_data to relevant backend storage.

        :param bytes encrypted_data: encrypted tomb data to be saved.

        :return: Success or failure
        :rtype: :class:`Bool`

        """

    @abc.abstractmethod
    def load(self) -> bytes:
        """Abstract method that must load encrypted_data from relevant backend storage.
        :return: the encrypted data
        :rtype: bytes

        """
