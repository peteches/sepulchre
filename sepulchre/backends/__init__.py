"""Defines classes for backend storage of sepulchre data.

Several backends are defined for common storage.

All backend classes *must* subclass :class:`sepulchre.backends.ABCBackend`

This defines the backend interface all backends *must* support.

the :class:`LocalDisk` class is the default and uses local disk storage.

Other backends will be provided at some point.

There is also the utility function :func:`load_backend` which will determine
the correct backend to load and will return an instance of that backend.

"""

from .abc_backend import ABCBackend
from .local_disk import LocalDiskBackend
