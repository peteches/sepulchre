"""Define backend for storing data to local disk."""

import logging
import os
import os.path
import stat

import nacl.hash

from sepulchre.backends.abc_backend import ABCBackend
from sepulchre.exceptions import TombPermissionError

LOGGER = logging.getLogger(__name__)


def setup():
    """Return appropriate local disk backend class."""
    return LocalDiskBackend


class LocalDiskBackend(ABCBackend):
    """Backend storage for local disk."""

    def __init__(self, tomb_name: str, config: dict = None):
        """Local disk storage class.

        The path on disk is detected using the configured `tomb_dir` value
        concatenated with a sha512 hash of the `tomb_name`.

        If the tomb directory does not exist then it will be created.

        If it does exist then a :class:`sepulchre.exceptions.TombPermissionError`
        exeption will be raised if the directory is readable by anyone other
        than the owner.

        """
        self.config = config or {}
        self.tomb_name = tomb_name
        self.tomb_name_hash = nacl.hash.sha512(bytes(self.tomb_name, "utf-8"))
        self.tomb_dir = self.config.get(
            "tomb_dir", os.path.expanduser("~/.sepulchre/tombs/")
        )
        self.path = os.path.join(self.tomb_dir, self.tomb_name_hash.decode("utf-8"))

        self._check_tomb_dir()

    def _check_tomb_dir(self):
        if not os.path.exists(self.tomb_dir):
            try:
                os.makedirs(self.tomb_dir, mode=0o0700, exist_ok=True)
            except OSError as exp:
                LOGGER.critical(
                    "Unable to create tomb directory: %s", self.tomb_dir, exc_info=exp
                )
                raise
            return True

        # tomb dir exists so lets check to see if we have
        # any insecure permissions that may let other people in.

        dir_perms = os.stat(self.tomb_dir).st_mode

        invalid_perms = [
            stat.S_IRGRP,
            stat.S_IWGRP,
            stat.S_IXGRP,
            stat.S_IROTH,
            stat.S_IWOTH,
            stat.S_IXOTH,
        ]

        for perm in invalid_perms:
            if dir_perms & perm:
                # little bit of bit manipulation to make octal
                # perms nicer to the user.
                nicer_dir_perms = dir_perms ^ ((dir_perms >> 9) << 9)
                raise TombPermissionError(
                    "Tomb permissions should be 700, found {:o}".format(nicer_dir_perms)
                )

        return True

    def save(self, encrypted_data: bytes) -> bool:
        """Save encrypted_data to disk.

        :param bytes encrypted_data: encrypted tomb data to be saved.self

        :return: Success or failure
        :rtype: :class:`bool`

        """
        try:
            with open(self.path, "wb") as tomb_file:
                tomb_file.write(encrypted_data)
        except OSError as exp:
            LOGGER.critical(
                "Unable to save %s to %s", self.tomb_name, self.path, exc_info=exp
            )

    def load(self) -> bytes:
        """Load encrypted_data from disk.

        :return: the encrypted data
        :rtype: bytes

        """
        try:
            with open(self.path, "rb") as tomb_file:
                return tomb_file.read()
        except OSError as exp:
            LOGGER.error(
                "Unable to load %s from %s", self.tomb_name, self.path, exc_info=exp
            )
