"""Tomb management module."""

import io

import nacl.hash
import nacl.signing
import yaml

from .backends.utils import load_backend
from .config import SepulchreConfig
from .user_interface import get_password


class Tomb(dict):
    """Tomb class manages secret data and its encryption."""

    def __init__(
        self, name: str, *args, config: SepulchreConfig = None, **kwargs
    ) -> None:
        """Create Tomb.

        :param str name: Name of the tomb
        :param dict config: Config dictionary.

        """
        self.name = name

        self.config = config or SepulchreConfig()

        pword = get_password()

        seed = nacl.hash.sha256(
            bytes(pword + self.name, "utf-8"), encoder=nacl.encoding.RawEncoder
        )

        private_key = nacl.signing.SigningKey(
            seed, encoder=nacl.encoding.RawEncoder
        ).to_curve25519_private_key()

        self.public_key = private_key.public_key
        self.box = nacl.public.Box(private_key, self.public_key)

        backend = load_backend(self.config.get("tomb", {}).get("backends", {}))
        self.backend = backend(self.name, self.config)

        enc_data = self.backend.load() or b""
        data = yaml.load(self.box.decrypt(enc_data))

        data.update(dict(*args, **kwargs))

        super().__init__(data)

    def save(self):
        """Encrypt data within the tomb and pass on to the backend for storage."""
        yaml_b = io.BytesIO()
        yaml.dump(dict(self), yaml_b, encoding="utf-8")
        enc_data = self.box.encrypt(yaml_b.getvalue(), encoder=nacl.encoding.RawEncoder)
        return self.backend.save(enc_data)
