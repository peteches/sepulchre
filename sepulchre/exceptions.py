"""All custom exception classes raised by sepulchre."""


class TombPermissionError(PermissionError):
    """Exception for when tomb permissions are wrong."""
