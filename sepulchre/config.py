"""Configuration related utilities and classes."""

import os

import yaml


class SepulchreConfig(dict):
    """Configuration class to manage all sepulchre config options.

    Should be treated like a dict.

    """

    def __init__(self, *args, **kwargs):
        """Initialise SepulchreConfig. """
        self.config_file = kwargs.pop("config_file", None)
        data = {}

        if self.config_file and os.path.exists(self.config_file):
            with open(self.config_file, "r") as file_desc:
                data = yaml.load(file_desc)

        data.update(**kwargs)
        for arg in args:
            data.update(arg)
        super().__init__(*args, **data)
