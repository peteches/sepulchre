PIPENV_VENV_IN_PROJECT=1
export PIPENV_VENV_IN_PROJECT

PATH:=${PATH}:${HOME}/.local/bin
export PATH

.venv:
	[[ -x "$$(which pipenv)" ]] || python3 -m pip install pipenv
	pipenv sync --dev

black: .venv
	[[ -x "$$(which black)" ]] || python3 -m pip install --user black
	pipenv run black .

isort: .venv
	pipenv run isort --apply --skip .venv

pylint: .venv
	pipenv run pylint --rcfile ./pylintrc sepulchre

pycodestyle: .venv
	pipenv run pycodestyle --max-line-length=88 sepulchre

nose2: .venv
	pipenv run nose2 --verbose

format: black isort

lint: pylint pycodestyle

test: nose2

all: format lint test

.PHONY: clean
clean:
	pipenv --rm
