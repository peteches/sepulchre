[![pipeline status](https://gitlab.com/peteches/sepulchre/badges/master/pipeline.svg)](https://gitlab.com/peteches/sepulchre/commits/master)
[![coverage report](https://gitlab.com/peteches/sepulchre/badges/master/coverage.svg)](https://gitlab.com/peteches/sepulchre/commits/master)

SEPULCHRE
==========
Sepulchre is a secrets management system written in python utilising Dan Bernsteins ed25519 deterministic Public key cryptography system.

Sepulchre is organised into two layers. The top level is the tomb.

Each tomb will contin a freeform structure of secrets, that should be thematically linked.

E.G. you may have one tomb for your gitlab secrets called gitlab.

Inside the gitlab tomb there will be a number of default secrets. The first is named password. As I expect most users will be using sepulchre as a password
management system initially this is the focus, however other secrets may also be defined. For instance you may want to keep any password recovery questions in sepulchre.

you could achieve this as below:

`sepulchre create_tomb gitlab` - sepulchre will prompt for a password to lock the tomb with

`sepulchre add_secret gitlab` - sepulchre will prompt for a password to unlock the gitlab tomb with and prompt for a secret and default to using 'password' as the key

`sepulcre add_secret gitlab mothers maiden name` - sepulchre will prompt for a password to unlock the gitlab tomb with sepulcre will  prompt for a secret using 'mothers maiden name' as the key

you may then retreive the secrets by

`sepulchre copy_secret gitlab` - sepulchre will prompt for a password to unlock the gitlab tomb with sepulchre will copy the secret to the clipboard. it will copy the password key by default

`sepulchre copy_secret gitlab mothers maiden name` - sepulchre will prompt for a password to unlock the gitlab tomb with sepulchre will copy the secret 'mothers maiden name' to the clipboard.

Each tomb can have a seperate password protecting it
