import nacl.hash
import nacl.signing


def gen_enc_box(key, name="bob"):

    seed = nacl.hash.sha256(
        bytes(key + name, "utf-8"), encoder=nacl.encoding.RawEncoder
    )

    sig_key = nacl.signing.SigningKey(seed)

    priv_key = sig_key.to_curve25519_private_key()
    pub_key = priv_key.public_key

    return nacl.public.Box(priv_key, pub_key)
