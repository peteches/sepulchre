# pylint doesn't like faker
# pylint: disable=no-member
import os
from unittest import mock

import faker
import nacl
import yaml
from nose2.tools import such

from sepulchre.backends.local_disk import LocalDiskBackend
from sepulchre.config import SepulchreConfig
from sepulchre.tomb import Tomb
from test_utils import gen_enc_box

with such.A("Tomb module") as it:

    @it.has_setup
    def global_setup():
        it.tomb_name = "test_tomb"
        it.fake = faker.Faker()
        it.plugin_dir = os.path.dirname(__file__)
        it.conf = SepulchreConfig(
            {
                "tomb": {
                    "backends": {
                        "name": "backend_test_plugin",
                        "plugin_dirs": [it.plugin_dir],
                    }
                }
            }
        )

        it.tomb_password = "SuperSecretPassword"

        it.enc_box = gen_enc_box(it.tomb_password, it.tomb_name)

    @it.has_test_setup
    def global_test_setup():
        it.load_patcher = mock.patch("sepulchre.tomb.load_backend", autospec=True)
        it.password_patcher = mock.patch(
            "sepulchre.tomb.get_password", return_value=it.tomb_password
        )

        it.test_dict = it.fake.pydict()

        test_dict_yaml = yaml.dump(it.test_dict, encoding="utf-8")

        enc_test_dict = it.enc_box.encrypt(test_dict_yaml)

        it.load_mock = it.load_patcher.start()
        it.password_mock = it.password_patcher.start()
        it.backend_mock = mock.MagicMock(spec=LocalDiskBackend)
        it.backend_mock.load.return_value = enc_test_dict
        it.load_mock.return_value = lambda s, x: it.backend_mock

    @it.has_test_teardown
    def global_test_teardown():
        mock.patch.stopall()

    with it.having("a Tomb class"):

        @it.should("initialise with just tomb_name")
        def tomb_initialise():

            tomb = Tomb(it.tomb_name)
            it.assertEqual(it.tomb_name, tomb.name)
            it.assertEqual(SepulchreConfig(), tomb.config)

        @it.should("initialise with optional config obj")
        def tomb_initialise_with_config():

            conf = SepulchreConfig(it.fake.pydict())
            tomb = Tomb(it.tomb_name, config=conf)
            it.assertEqual(it.tomb_name, tomb.name)
            it.assertEqual(conf, tomb.config)

        @it.should("load specified backend")
        def tomb_load_backend():

            Tomb(it.tomb_name, config=it.conf)

            it.load_mock.assert_called_with(it.conf["tomb"]["backends"])

        @it.should("behave like a dict")
        def tomb_bahaves_as_dict():
            tomb = Tomb(it.tomb_name, config=it.conf)

            it.assertIsInstance(tomb, dict)

            tomb_kwargs = Tomb(it.tomb_name, config=it.conf, **it.test_dict)
            tomb_arg = Tomb(it.tomb_name, it.test_dict, config=it.conf)

            for key, value in it.test_dict.items():
                it.assertEqual(tomb_kwargs[key], value)
                it.assertEqual(tomb_kwargs.get(key), value)
                it.assertEqual(tomb_arg[key], value)
                it.assertEqual(tomb_arg.get(key), value)

        @it.should("create a pynacl encryption box")
        def tomb_enc_box():
            tomb = Tomb(it.tomb_name)

            it.assertIsInstance(tomb.box, nacl.public.Box)
            # if password not being called then only encrypting
            # based on tomb name which is much easier to obtain.
            it.password_mock.assert_called_once()

        @it.should("be able to save and only pass encrypted data to the backend")
        def tomb_save_enc_data():
            tomb = Tomb(it.tomb_name, config=it.conf)

            tomb.save()

            enc_data = it.backend_mock.save.call_args[0][0]

            plain_text = tomb.box.decrypt(enc_data)

            it.assertEqual(plain_text, yaml.dump(dict(tomb), encoding="utf-8"))

        @it.should("load any data from the backend.")
        def tomb_load_data():

            tomb = Tomb(it.tomb_name)

            for key, value in it.test_dict.items():
                it.assertEqual(tomb[key], value)

        @it.should("overwrite loaded data with invocation args")
        def tomb_load_invocation():

            overwrite = {}
            for key, value in it.test_dict.items():
                overwrite[key] = it.fake.pystr()

            tomb = Tomb(it.tomb_name, **overwrite)

            for key, value in overwrite.items():
                it.assertEqual(tomb[key], value)


it.createTests(globals())
