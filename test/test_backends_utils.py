"""Testing backends functionality

"""
# pylint doesn't like faker
# pylint: disable=no-member
import os
import sys

import faker
from nose2.tools import such
from pyfakefs.fake_filesystem_unittest import Patcher

from sepulchre.backends import utils

with such.A("Backend storage package") as it:

    @it.has_test_setup
    def global_test_setup():
        it.fake = faker.Faker()
        it.fspatch = Patcher()
        it.fspatch.setUp()
        it.plugin_dir = os.path.dirname(__file__)

    @it.has_test_teardown
    def global_test_teardown():
        it.fspatch.tearDown()

    with it.having("A utils module"):

        with it.having("a load_backend function"):

            @it.should(
                "default to returning a sepulchre.backends.LocalDiskBackend class"
            )
            def utils_load_backend():
                test_class = utils.load_backend()
                old_path = sys.path
                sys.path.append(os.path.join(__file__, "../sepulchre/backends"))
                import local_disk  # pylint: disable=import-error

                sys.path = old_path
                it.assertEqual(test_class, local_disk.LocalDiskBackend)

            @it.should("accept a config dict to specify a different backend to return")
            def utils_load_test_backend():

                old_path = sys.path
                sys.path.append(it.plugin_dir)

                import backend_test_plugin

                conf = {"name": "backend_test_plugin", "plugin_dirs": [it.plugin_dir]}
                test_class = utils.load_backend(conf)

                sys.path = old_path
                it.assertEqual(test_class, backend_test_plugin.TestBackend)


it.createTests(globals())
