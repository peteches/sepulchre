# pylint doesn't like the faker library
# pylint: disable=no-member
import faker
import yaml
from nose2.tools import such
from pyfakefs.fake_filesystem_unittest import Patcher

from sepulchre.config import SepulchreConfig

with such.A("Config module") as it:

    @it.has_setup
    def global_setup():
        it.fake = faker.Faker()

    @it.has_test_setup
    def global_test_setup():
        it.fakefs = Patcher()
        it.fakefs.setUp()
        it.data = it.fake.pydict()

        yaml_data = yaml.dump(it.data)
        it.config_filepath = it.fake.file_path(extension="yml")
        it.fakefs.fs.create_file(
            it.config_filepath, contents=yaml_data, create_missing_dirs=True
        )

    @it.has_test_teardown
    def global_test_teardown():
        it.fakefs.tearDown()

    with it.having("A SepulchreConfig class"):

        @it.should("be initialised like a dict")
        def config_dict_initialise():
            iterable_conf = SepulchreConfig(it.data)

            for key, value in it.data.items():
                it.assertEqual(iterable_conf[key], value)

            for key, value in iterable_conf.items():
                it.assertEqual(it.data[key], value)

            kwarg_conf = SepulchreConfig(**it.data)

            for key, value in it.data.items():
                it.assertEqual(kwarg_conf[key], value)

            for key, value in kwarg_conf.items():
                it.assertEqual(it.data[key], value)

        @it.should("be updateable like a dict")
        def config_dict_update():
            conf = SepulchreConfig(it.data)
            key = it.fake.pystr()
            value = it.fake.pystr()
            conf[key] = value
            it.assertEqual(conf[key], value)

            updates = it.fake.pydict()
            for key in conf.keys():
                updates[key] = it.fake.pystr()

            conf.update(updates)

            for key, value in updates.items():
                it.assertEqual(conf[key], value)

        @it.should(
            "accept a config file path to load data from but overrides with passed args."
        )
        def config_file_load():
            conf = SepulchreConfig(config_file=it.config_filepath)

            for key, value in it.data.items():
                it.assertEqual(conf[key], value)

            overrides = {
                key: it.fake.pystr() for key in it.data.keys() if it.fake.pybool()
            }

            over_conf = SepulchreConfig(overrides, config_file=it.config_filepath)

            for key, value in over_conf.items():
                if key in overrides.keys():
                    it.assertEqual(overrides[key], value)
                else:
                    it.assertEqual(it.data[key], value)


it.createTests(globals())
