"""Testing backends functionality

"""
import os.path

import nacl.hash
from nose2.tools import such
from pyfakefs.fake_filesystem_unittest import Patcher

from sepulchre import backends
from sepulchre.exceptions import TombPermissionError
from test_utils import gen_enc_box

with such.A("Backend storage package") as it:

    @it.has_test_setup
    def global_test_setup():
        it.fspatch = Patcher()
        it.fspatch.setUp()

        it.tomb_name = "test_tomb"
        it.tomb_name_hash = nacl.hash.sha512(bytes(it.tomb_name, "utf-8"))
        it.tomb_dir = os.path.expanduser("~/.sepulchre/tombs")

        it.key = "RandomString"

        it.enc_box = gen_enc_box(it.key)
        it.msg = b"Hello I'm a super secret string"
        it.enc_data = it.enc_box.encrypt(it.msg)

    @it.has_test_teardown
    def global_test_teardown():
        it.fspatch.tearDown()

    with it.having("A local disk backend module"):

        @it.should("have a setup function")
        def ld_setup():
            ld_class = backends.local_disk.setup()
            it.assertEqual(backends.LocalDiskBackend, ld_class)

        with it.having("a LocalDiskBackend class"):

            @it.should("inherit from ABCBackend")
            def ld_is_abc():
                ldb = backends.LocalDiskBackend(it.tomb_name)
                it.assertIsInstance(ldb, backends.ABCBackend)

            @it.should("create the tomb_dir if it is missing")
            def ld_create_tomb_dir():
                backends.LocalDiskBackend(it.tomb_name)
                it.assertTrue(os.path.exists(it.tomb_dir))
                it.assertEqual(os.stat(it.tomb_dir).st_mode, 0o40700)

            @it.should("raise an exception if tomb_dir permissions are wrong")
            def ld_tomb_dir_permission():
                wrong_perms = [
                    0o710,
                    0o720,
                    0o740,
                    0o750,
                    0o760,
                    0o770,
                    0o701,
                    0o702,
                    0o704,
                    0o705,
                    0o706,
                    0o707,
                    0o711,
                    0o722,
                    0o744,
                    0o755,
                    0o766,
                    0o777,
                ]

                for perms in wrong_perms:
                    it.fspatch.fs.create_dir(it.tomb_dir + str(perms), perms)
                    msg = "Tomb permissions should be 700, found {:o}".format(perms)
                    with it.assertRaises(TombPermissionError, msg=msg):
                        backends.LocalDiskBackend(
                            it.tomb_name, {"tomb_dir": it.tomb_dir + str(perms)}
                        )

            @it.should("save a stream of bytes to disk. (default location)")
            def ld_save_method_default():
                it.fspatch.fs.create_dir(it.tomb_dir, 0o0700)
                ldb = backends.LocalDiskBackend(it.tomb_name, {})
                ldb.save(it.enc_data)

                tomb_file = os.path.join(it.tomb_dir, it.tomb_name_hash.decode("utf-8"))

                it.assertTrue(os.path.exists(tomb_file))

                with open(tomb_file, "rb") as file:
                    it.assertEqual(it.msg, it.enc_box.decrypt(file.read()))

            @it.should("save a stream of bytes to disk. (custom location)")
            def ld_save_method_custom():
                it.fspatch.fs.create_dir("sepulchre", 0o0700)
                ldb = backends.LocalDiskBackend(it.tomb_name, {"tomb_dir": "sepulchre"})
                ldb.save(it.enc_data)

                tomb_file = os.path.join("sepulchre", it.tomb_name_hash.decode("utf-8"))

                it.assertTrue(os.path.exists(tomb_file))

                with open(tomb_file, "rb") as file:
                    it.assertEqual(it.msg, it.enc_box.decrypt(file.read()))

            @it.should("load a stream of bytes from disk (default location)")
            def ld_load_method_default():
                tomb_file = os.path.join(it.tomb_dir, it.tomb_name_hash.decode("utf-8"))

                it.fspatch.fs.create_dir(it.tomb_dir, 0o0700)

                it.fspatch.fs.create_file(
                    tomb_file,
                    contents=it.enc_data,
                    create_missing_dirs=True,
                    st_mode=0o0600,
                )

                ldb = backends.LocalDiskBackend(it.tomb_name)

                it.assertEqual(it.msg, it.enc_box.decrypt(ldb.load()))

            @it.should("load a stream of bytes from disk (custom location)")
            def ld_load_method_custom():
                it.fspatch.fs.create_dir("sepulchre", 0o0700)
                tomb_file = os.path.join("sepulchre", it.tomb_name_hash.decode("utf-8"))

                it.fspatch.fs.create_file(
                    tomb_file,
                    contents=it.enc_data,
                    create_missing_dirs=True,
                    st_mode=0o0600,
                )

                ldb = backends.LocalDiskBackend(it.tomb_name, {"tomb_dir": "sepulchre"})

                it.assertEqual(it.msg, it.enc_box.decrypt(ldb.load()))


it.createTests(globals())
