import sepulchre.backends


class TestBackend(sepulchre.backends.ABCBackend):
    def __init__(self, tomb_name, config=None):
        self.name = tomb_name
        self.config = config or {}

    def save(self, enc_data):
        return True

    def load(self):
        return b"enc_data"


def setup():
    return TestBackend
