"""Testing backends functionality

"""
import os.path

import nacl.hash
import nacl.signing
from nose2.tools import such
from pyfakefs.fake_filesystem_unittest import Patcher

import sepulchre.exceptions
from sepulchre import backends


def gen_enc_box(key):

    seed = nacl.hash.sha256(bytes(key, "utf-8"), encoder=nacl.encoding.RawEncoder)

    sig_key = nacl.signing.SigningKey(seed)

    priv_key = sig_key.to_curve25519_private_key()
    pub_key = priv_key.public_key

    return nacl.public.Box(priv_key, pub_key)


with such.A("Backend storage package") as it:

    @it.has_test_setup
    def global_test_setup():
        it.fspatch = Patcher()
        it.fspatch.setUp()

        it.tomb_name = "test_tomb"
        it.tomb_name_hash = nacl.hash.sha512(bytes(it.tomb_name, "utf-8"))
        it.tomb_dir = os.path.expanduser("~/.sepulchre/tombs")

        it.key = "RandomString"

        it.enc_box = gen_enc_box(it.key)
        it.msg = b"Hello I'm a super secret string"
        it.enc_data = it.enc_box.encrypt(it.msg)

    @it.has_test_teardown
    def global_test_teardown():
        it.fspatch.tearDown()

    with it.having("A abstract base class"):

        @it.should("have a save abstract method")
        def abc_save_method():
            it.assertIn("save", backends.ABCBackend.__dict__["__abstractmethods__"])

        @it.should("have a load abstract method")
        def abc_load_method():
            it.assertIn("load", backends.ABCBackend.__dict__["__abstractmethods__"])


it.createTests(globals())
